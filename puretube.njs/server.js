

const host = 'localhost';
const port = 8000;

require("http")
	.createServer(
		// HACK to access a module from an app
		(() => {
			if (global.tube)
				throw 'aberant state encountered';
			var site = []
			const tube = (main) =>
			{
				return () =>
				{
					if (0 != site.length)
						throw 'site has already been specified';
					site = [main]
				}
			}
			global.tube = tube;
			require("./target/tube-app.js");
			if (tube != global.tube)
				throw 'aberant stuff happened';
			if (1 != site.length)
				throw 'site was not specified';
			return site[0];
		})()
	)
	.listen(
		port,
		host,
		() => console.log(`Server is running on http://${host}:${port}`)
	);
