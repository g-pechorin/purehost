
"use strict";




// foreign import _pure_ :: forall a. a -> Request a
export const      _pure_ =           (a)=>
{
	return (req, res) => a;
}

// foreign import _map_ :: forall a b. (a -> b) -> Request a -> Request b
export const      _map_ =              (f)      =>(a)        =>
{
	return (req, res) =>
	{
		throw '??_map_??';
	}
}

// foreign import _apply_ :: forall a b. Request (a -> b) -> Request a -> Request b
export const      _apply_ =              (f)              => (a)        =>
{
	return (req, res) =>
	{
		throw '??_apply_??';
	}
}


// foreign import _bind_ :: forall a b. Request a -> (a -> Request b) -> Request b
export const      _bind_ =              (a)       => (f)              =>
{
	return (req, res) =>
	{
		var a0 = a(req, res);
		var f0 = f(a0);
		return f0(req, res);
	}
}

// -- most-basic form of the/a wepage
// foreign import end :: Int    -> String -> Request Unit
export const      end =  (code) => (text) =>
{
	return (req, res) =>
	{
		res.writeHead(code);
		res.end(text);
	}
}

// -- what's the path
// foreign import requestPathName :: Request String
export const      requestPathName = (req, res) => require('url').parse(req.url, true).pathname

/*
Url {                                
  protocol: null,                    
  slashes: null,                     
  auth: null,                        
  host: null,                        
  port: null,                        
  hostname: null,                    
  hash: null,                        
  search: null,                      
  query: [Object: null prototype] {},
  pathname: '/',                     
  path: '/',                         
  href: '/'                          
}                                    
*/