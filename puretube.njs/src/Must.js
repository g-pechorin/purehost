
"use strict";

// foreign import data DataBind :: Type

// foreign import blank :: DataBind
export const blank = () => {}

// foreign import put ::  DataBind -> String -> String -> DataBind
export const put = (into) => (key) => (val) => () =>
{
	var data = into();

	if (data[key])
		throw ('key collision on {'+key+'}!')
	
	data[key] = val;
	return data;
}

const fs = require('fs')
const mustache = require('mustache')

// // foreign import load :: String -> DataBind -> String
// export const load = (path) => (data) =>
// {

// 	fs.readFileSync(process.cwd() + '/' + path, 'utf8')

// 	console.log(path);
// 	console.log(path);
// 	console.log(path);
// 	console.log(path);

// 	throw 'now what do?';
// }

// foreign import send :: Int -> String -> DataBind -> Request Unit
export const send = (code) => (mime) => (path) => (data) =>
{
	return (req, res) =>
	{
		res.setHeader('Content-Type', mime)
		res.writeHead(code);
		res.end(mustache.render(fs.readFileSync(process.cwd() + '/' + path, 'utf8'), data()))
	}
}

