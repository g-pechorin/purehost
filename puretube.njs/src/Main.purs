module Main where

import Prelude
import Host 
import Must

import Effect (Effect)

foreign import site :: Request Unit -> Effect Unit

main :: Effect Unit
main = do
  site tube

tube :: Request Unit
tube = do
  path <- requestPathName
  -- end 200 $ "all tidy now i imagine >" <> path <> "<"
  -- end 200 $ "all tidy now i imagine >" <> path <> "<"
  send 200 "text/html" "mus/main.mus" blank
