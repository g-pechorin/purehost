module Must where

import Host (Request)
import Prelude (Unit)

foreign import data DataBind :: Type

foreign import blank :: DataBind
foreign import put ::  DataBind -> String -> String -> DataBind
-- foreign import load :: String -> DataBind -> String
foreign import send :: Int -> String -> String -> DataBind -> Request Unit

