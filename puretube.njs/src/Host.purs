module Host where


import Prelude

foreign import data Request :: Type -> Type

foreign import _pure_ :: forall a. a -> Request a
foreign import _map_ :: forall a b. (a -> b) -> Request a -> Request b
foreign import _apply_ :: forall a b. Request (a -> b) -> Request a -> Request b
foreign import _bind_ :: forall a b. Request a -> (a -> Request b) -> Request b

instance Applicative Request where
  pure :: forall a. a -> Request a
  pure = _pure_

instance Functor Request where
  map :: forall a b. (a -> b) -> Request a -> Request b
  map = _map_

instance Apply Request where
  apply :: forall a b. Request (a -> b) -> Request a -> Request b
  apply = _apply_

instance Bind Request where
  bind :: forall a b. Request a -> (a -> Request b) -> Request b
  bind = _bind_

-- most-basic form of the/a wepage
foreign import end :: Int -> String -> Request Unit

-- what's the path
foreign import requestPathName :: Request String
